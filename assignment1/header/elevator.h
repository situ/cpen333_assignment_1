#pragma once
#ifndef __Elevator__
#define __Elevator__

#define up 1
#define down 0
#define closed 1
#define open 0

//#include "rt.h"
#include "E:\UBC\2020-2021\CPEN 333\Assignments\rt.h"
class Elevator {
private:
	struct elStatus {
		int floorNum;
		int direction;
		bool doorOpen;
		bool fault;
		int numPassengers;
	};
	CDataPool* elDataPool;
	CMutex* elMutex;
	elStatus* elPointer;

public:
	int Get_Elevator_Floor(){
		elMutex->Wait();
		return elPointer->floorNum;
		elMutex->Signal();
	}
	int Get_Elevator_Direction() {
		elMutex->Wait();
		return elPointer->direction;
		elMutex->Signal();
	}
	BOOL Get_Elevator_Door() {
		elMutex->Wait();
		return elPointer->doorOpen;
		elMutex->Signal();
	}
	BOOL Get_Elevator_Fault() {
		elMutex->Wait();
		return elPointer->fault;
		elMutex->Signal();
	}
	int Get_Elevator_Passengers() {
		elMutex->Wait();
		return elPointer->numPassengers;
		elMutex->Signal();
	}

	void Update_Elevator_Floor(int floor) {
		elMutex->Wait();
		elPointer->floorNum = floor;
		elMutex->Signal();
	}
	void Update_Elevator_Direction(int direction) {
		elMutex->Wait();
		elPointer->direction = direction;
		elMutex->Signal();
	}
	void Update_Elevator_Door(bool doorOpen) {
		elMutex->Wait();
		elPointer->doorOpen = doorOpen;
		elMutex->Signal();
	}
	void Update_Elevator_Fault(bool fault) {
		elMutex->Wait();
		elPointer->fault = fault;
		elMutex->Signal();
	}
	void Update_Elevator_numPassengers(int numPassengers) {
		elMutex->Wait();
		elPointer->numPassengers = numPassengers;
		elMutex->Signal();
	}

	// constructor and destructor
	Elevator(string Name) {
		elMutex = new CMutex(string("__Mutex__") + string(Name));
		elDataPool = new CDataPool(string("__DataPool__") + string(Name), sizeof(struct elStatus));
		elPointer = (struct elStatus*)(elDataPool->LinkDataPool());
	}
	~Elevator() { /* delete mutex and datapool; */ }
};

#endif
