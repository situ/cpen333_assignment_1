//#include "C:\Users\imhen\Google Drive (hwhsitu@gmail.com)\CPEN_333\Assignments\Assignment_1\assignment1\header\rt.h"
//#include "C:\Users\imhen\Google Drive (hwhsitu@gmail.com)\CPEN_333\Assignments\Assignment_1\assignment1\header\elevator.h"

#include "E:\UBC\2020-2021\CPEN 333\Assignments\rt.h"
#include "E:\UBC\2020-2021\CPEN 333\Assignments\Assignment1\assignment1\header\elevator.h"

#define up 1
#define down 0
#define closed 1
#define open 0
#define END 1000
#define startFault 2000
#define endFault 2001

int main(void) {
	Elevator E1("Elevator1");
	CMailbox El1MailBox;
	UINT	message = 10;		// A variable to holds message received in message queue
	int nextFloor;
	CSemaphore		ps1("PS1", 0, 1);    // semaphore with initial value 0 and max value 1
	CSemaphore		cs1("CS1", 1, 1);    // semaphore with initial value 1 and max value 1
	CSemaphore		ps2("PS2", 0, 1);    // semaphore with initial value 0 and max value 1
	CSemaphore		cs2("CS2", 1, 1);    // semaphore with initial value 1 and max value 1

	do {
		if (El1MailBox.TestForMessage() == TRUE) {
			message = El1MailBox.GetMessageA();
			if (message == END)
			{
				return 0;
			}
			else if (message == startFault)
			{// if fault message is received loop until endFault is received
				cs1.Wait();
				cs2.Wait();
				E1.Update_Elevator_Fault(true);
				ps1.Signal();
				ps2.Signal();
				do {
					if (El1MailBox.TestForMessage() == TRUE) {
						message = El1MailBox.GetMessageA();
						Sleep(1);
					}
				} while (message != endFault);
				cs1.Wait();
				cs2.Wait();
				E1.Update_Elevator_Fault(false);
				ps1.Signal();
				ps2.Signal();
			}
			else if (0 <= message && message <= 9)
			{
				nextFloor = message;
				cs1.Wait();
				cs2.Wait();
				if (nextFloor > E1.Get_Elevator_Floor()) // going up
				{
					E1.Update_Elevator_Direction(up);
				}
				else if (nextFloor < E1.Get_Elevator_Floor())
				{
					E1.Update_Elevator_Direction(down);
				}
				ps1.Signal();
				ps2.Signal();

				cs1.Wait();
				cs2.Wait();
				if (E1.Get_Elevator_Door() == open)
				{
					E1.Update_Elevator_Door(false);
					Sleep(50); // wait for doors to close
				}
				ps1.Signal();
				ps2.Signal();
				Sleep(500); // travelling 
				cs1.Wait();
				cs2.Wait();
				E1.Update_Elevator_Door(open);
				E1.Update_Elevator_Floor(nextFloor);
				ps1.Signal();
				ps2.Signal();
				Sleep(50);	// People walking out/in
				cs1.Wait();
				cs2.Wait();
				E1.Update_Elevator_Door(false);
				ps1.Signal();
				ps2.Signal();
			}
		}
		Sleep(1);
	} while (message != END);

	return 0;
}