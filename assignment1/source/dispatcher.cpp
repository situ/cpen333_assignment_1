#pragma once
#include "C:\Users\imhen\Google Drive (hwhsitu@gmail.com)\CPEN_333\Assignments\Assignment_1\assignment1\header\rt.h"
#include "C:\Users\imhen\Google Drive (hwhsitu@gmail.com)\CPEN_333\Assignments\Assignment_1\assignment1\header\elevator.h"

//#include "E:\UBC\2020-2021\CPEN 333\Assignments\rt.h"
//#include "E:\UBC\2020-2021\CPEN 333\Assignments\Dispatcher\Elevator1\elevator.h"

#include <queue>
#include <string>
#include <ctype.h>
#include <stdio.h>
#include <iostream> 

#define up 1
#define down 0
#define closed 1
#define open 0
#define END 1000
#define startFault 2000
#define endFault 2001

using namespace std;
const int elNumFloors = 10;
int elevatorID, requestFloor, direction, toFloor;
char requestDirection[1];
int elevator1_in_request[elNumFloors] = { 0,0,0,0,0,0,0,0,0,0 };
int elevator2_in_request[elNumFloors] = { 0,0,0,0,0,0,0,0,0,0 };
int out_request_floor_up[elNumFloors] = { 0,0,0,0,0,0,0,0,0,0 };
int out_request_floor_down[elNumFloors] = { 0,0,0,0,0,0,0,0,0,0 };

//struct elevatorData {
//	int floorNum;
//	int direction;
//	bool doorOpen;
//	bool fault;
//};
//struct elevatorData* elevator1Ptr;
//struct elevatorData* elevator2Ptr;

char command[2];

Elevator E1("Elevator1");
Elevator E2("Elevator2");

int E1Floor;

int floorNum_el1;
int direction_el1;
bool doorOpen_el1;
bool fault_el1;

int floorNum_el2;
int direction_el2;
bool doorOpen_el2;
bool fault_el2;

//const int elFloorOrderSize = 10;
//int el1FloorOrder[elFloorOrderSize];	// a list of floors to travel to for elevator 1
//int el1FloorSize;		// the current number of floors in the list to travel to
//int el2FloorOrder[elFloorOrderSize];	// a list of floors to travel to for elevator 2
//int el2FloorSize;		// the current number of floors in the list to travel to


UINT __stdcall getElevator1Status(void* args)
{
	CSemaphore		ps2("PS2", 0, 1);    // semaphore with initial value 0 and max value 1
	CSemaphore		cs2("CS2", 1, 1);    // semaphore with initial value 1 and max value 1
	while (1)
	{
		ps2.Wait();
		floorNum_el1 = E1.Get_Elevator_Floor();
		direction_el1 = E1.Get_Elevator_Direction();
		doorOpen_el1 = E1.Get_Elevator_Door();
		fault_el1 = E1.Get_Elevator_Fault();
		cs2.Signal();
	}
	return 0;
}

UINT __stdcall getElevator2Status(void* args)
{
	CSemaphore		ps4("PS4", 0, 1);    // semaphore with initial value 0 and max value 1
	CSemaphore		cs4("CS4", 1, 1);    // semaphore with initial value 1 and max value 1
	while (1)
	{
		floorNum_el2 = E2.Get_Elevator_Floor();
		direction_el2 = E2.Get_Elevator_Direction();
		doorOpen_el2 = E2.Get_Elevator_Door();
		fault_el2 = E2.Get_Elevator_Fault();
	}
	return 0;
}

UINT __stdcall pipeline(void* args)
{
	CPipe pipe1("MyPipe", 1024);
	char str[256];
	while (1)
	{
		pipe1.Read(&str, sizeof(str));
		cout << str << endl;
		str[2] = str[1];
		str[1] = ' '; // seperate two string
		str[3] = NULL; // end of string

		if (isdigit(str[0])) {
			sscanf_s(str, "%d%d", &elevatorID, &requestFloor);
			//cout << "elevatorID: " << elevatorID << endl;
			//cout << "requestFloor: " << requestFloor << endl;
			if (elevatorID == 1)
			{
				elevator1_in_request[requestFloor] = 1;
			}
			else if (elevatorID == 2)
			{
				elevator2_in_request[requestFloor] = 1;
			}
		}
		else if (isalpha(str[0])) {
			sscanf_s(str, "%s%d", &requestDirection, &requestFloor);
			if (strcmp(requestDirection, "u") == 0) {
				direction = up; // up
				out_request_floor_up[toFloor] = 1;
			}

			else if (strcmp(requestDirection, "d") == 0) {
				direction = down; // down
				out_request_floor_down[toFloor] = 1;
			}

			else if (strcmp(requestDirection, "e") == 0) { // e for end process
				break;
			}
		}
		else
			printf("Error: Invalid format, input again.\n");
	}

	return 0;
}

int main()
{

	CProcess e1("E:\\UBC\\2020-2021\\CPEN 333\\Assignments\\Dispatcher\\Debug\\Elevator1.exe",	// pathlist to child program executable				
		NORMAL_PRIORITY_CLASS,			// priority
		PARENT_WINDOW,						// process has its parent window					
		ACTIVE
	);
	CProcess p1("E:\\UBC\\2020-2021\\CPEN 333\\Assignments\\Dispatcher\\Debug\\IO.exe",
		NORMAL_PRIORITY_CLASS,			// priority
		OWN_WINDOW,						// process has its own window					
		ACTIVE							// process is active immediately
	);

	e1.Resume();
	p1.Resume();

	CMailbox El1MailBox;

	CThread	elevator1thread(getElevator1Status, ACTIVE, NULL);
	CThread	elevator2thread(getElevator2Status, ACTIVE, NULL);

	// IO Pipeline
	CPipe pipe1("MyPipe", 1024);
	char str[256];
	CThread IOthread(pipeline, ACTIVE, NULL);

	int el1ClosestLower = 0;
	int el1ClosestUpper = 0;
	bool el1HasLower = false;
	bool el1HasUpper = false;
	bool el1Empty = true;

	int el2ClosestLower = 0;
	int el2ClosestUpper = 0;
	bool el2HasLower = false;
	bool el2HasUpper = false;
	bool el2Empty = true;

	do {
		if (elevator1_in_request[floorNum_el1] == 1)	// If the elevator has arrived to a floor on its list
		{
			elevator1_in_request[floorNum_el1] = 0; // remove floor from the list
			// determine shortest travel distance from current floor
			for (int i = 0; i < elNumFloors; i++)
			{
				if (elevator1_in_request[i] == 1)
				{
					if (i < floorNum_el1)
					{
						el1HasLower = true;
						el1ClosestLower = i;
					}
					else if (i > floorNum_el1 && el1HasUpper == false)
					{
						el1HasUpper = true;
						el1ClosestUpper = i;
					}
				}
			}
			if (el1HasLower == true && el1HasUpper == true)
			{
				if (floorNum_el1 - el1ClosestLower < el1ClosestUpper - floorNum_el1)
				{
					e1.Post(el1ClosestLower);
				}
				else if (floorNum_el1 - el1ClosestLower > el1ClosestUpper - floorNum_el1)
				{
					e1.Post(el1ClosestUpper);
				}
			}
			else if (el1HasLower == true)
			{
				e1.Post(el1ClosestLower);
			}
			else if (el1HasUpper == true)
			{
				e1.Post(el1ClosestUpper);
			}
			else {
				el1Empty = true; // the elevator doesn't have anymore floors to travel to
			}
		}
		// Space for elevator 2

		// See if there are any elevator requests
		for (int j = 0; j < elNumFloors; j++)
		{
			if (out_request_floor_up[j] == 1 || out_request_floor_down[j] == 1) // if there are any floor requests on j
			{
				if (el1Empty) // if elevator 1 is empty add it to its list
				{
					elevator1_in_request[j] = 1;
				}
				else if (el2Empty) // if elevator 2 is empty add it to its list
				{
					elevator2_in_request[j] = 1;
				}
				else // determine the closest elevator
				{
					if (abs(floorNum_el1 - j) < abs(floorNum_el2 - j))
					{
						elevator1_in_request[j] = 1;
					}
					else
					{
						elevator2_in_request[j] = 1;
					}
				}
			}
		}
	} while (1);

	IOthread.WaitForThread();
	//

	return 0;
}