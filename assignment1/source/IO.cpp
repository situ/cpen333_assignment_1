//#include "C:\Users\imhen\Google Drive (hwhsitu@gmail.com)\CPEN_333\Assignments\Assignment_1\assignment1\header\rt.h"
//#include "C:\Users\imhen\Google Drive (hwhsitu@gmail.com)\CPEN_333\Assignments\Assignment_1\assignment1\header\elevator.h"

#include "E:\UBC\2020-2021\CPEN 333\Assignments\rt.h"
#include "E:\UBC\2020-2021\CPEN 333\Assignments\Assignment1_TestLocal\Elevator\elevator.h"

#include <stdio.h>
#include <iostream> 

using namespace std;
Elevator E1("Elevator1");
Elevator E2("Elevator2");
CMutex	 M1("MyMutex");

struct elevatorData {
	int floorNum;
	int direction;
	bool doorOpen;
	bool fault;
};

//void Draw_Elevator(int ID, int width, int height, int dir)
//{
//	MOVE_CURSOR(60, 10 + dir);
//	cout << ".." << endl;
//	//cout<<"Elevator #"<<ID<< "at floor"<<
//}

int floorNum_el1;
int direction_el1;
bool doorOpen_el1;
bool fault_el1;

int floorNum_el2;
int direction_el2;
bool doorOpen_el2;
bool fault_el2;

void Print_Status(int ID, int floorNum, int Direction, int doorStatus)
{
	MOVE_CURSOR(60 + (ID - 1) * 60, 10);
	cout << "Elevator #" << ID + 1 << endl;
	cout << "At floor #" << floorNum + 1 << endl;
	cout << "Going " << Direction << endl;
	cout << "Door " << doorStatus << endl;
}

UINT __stdcall getElevator1Status(void* args)
{
	while (1)
	{
		floorNum_el1 = E1.Get_Elevator_Floor();
		direction_el1 = E1.Get_Elevator_Direction();
		doorOpen_el1 = E1.Get_Elevator_Door();
		fault_el1 = E1.Get_Elevator_Fault();
		Print_Status(1, floorNum_el1, direction_el1, doorOpen_el1, fault_el1);
	}
	return 0;
}

UINT __stdcall getElevator2Status(void* args)
{
	while (1)
	{
		floorNum_el2 = E2.Get_Elevator_Floor();
		direction_el2 = E2.Get_Elevator_Direction();
		doorOpen_el2 = E2.Get_Elevator_Door();
		fault_el2 = E2.Get_Elevator_Fault();
		Print_Status(2, floorNum_el2, direction_el2, doorOpen_el2, fault_el2);
	}
	return 0;
}

int main(void)
{
	UINT	message = 0;		// A variable to holds message received in message queue

	CPipe pipe1("MyPipe", 1024);
	char str[256];
	printf("IO stream starts ... ... \n");

	CThread	elevator1thread(getElevator1Status, ACTIVE, NULL);
	CThread	elevator2thread(getElevator2Status, ACTIVE, NULL);

	printf("If requesting from outside, follow the format: \n first character: direction: (u OR d)\n second character: destination floor (range 0 to 9)\n");
	printf("if requesting from inside, follow the format.\n first character: Elevator ID (1 OR 2)\n second character: destination floor (range 0 to 9)\n");
	while (1)
	{
		cin >> str;
		pipe1.Write(&str, sizeof(str));
		Sleep(100);
		cout << "moving... \n";
	}
	return 0;
}